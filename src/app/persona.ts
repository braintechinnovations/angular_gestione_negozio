export class Persona {
    id: number | undefined;
    nome: string | undefined;
    cognome: string | undefined;
    cod_fis: string | undefined;
}
