import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Carta } from '../carta';
import { CartaService } from '../carta.service';

@Component({
  selector: 'app-carta-insert',
  templateUrl: './carta-insert.component.html',
  styleUrls: ['./carta-insert.component.css']
})
export class CartaInsertComponent implements OnInit {

  varNegozio: any;
  varCodice: any;

  constructor(
    private service: CartaService,
    private router: Router
    ) { }

  ngOnInit(): void {
  }

  inserisciCarta(){
    var temp = new Carta();
    temp.negozio = this.varNegozio;
    temp.numero = this.varCodice;

    this.service.insertCarta(temp).subscribe(
      (risultato) => {
        if(risultato){
          //TODO: alert
          this.router.navigateByUrl("carta/lista")
        }
      },
      (errore) => {
        
      }
    )
  }

}
