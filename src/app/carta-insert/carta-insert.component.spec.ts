import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartaInsertComponent } from './carta-insert.component';

describe('CartaInsertComponent', () => {
  let component: CartaInsertComponent;
  let fixture: ComponentFixture<CartaInsertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartaInsertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartaInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
