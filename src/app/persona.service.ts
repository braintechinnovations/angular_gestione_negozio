import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Persona } from './persona';

const endpoint = "http://localhost:8888/persona"

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  constructor(private http: HttpClient) { }

  insertPersona(objPersona: Persona){
    var custom_header = new HttpHeaders();
    custom_header = custom_header.set("Content-Type", "application/json");

    return this.http.post<Persona>(
      endpoint + "/inserisci", 
      JSON.stringify(objPersona), 
      {headers: custom_header}
      );
  }

  findAllPersona(){
    return this.http.get(endpoint + "/")
  }

  findByIdPersona(idPersona: number){
    return this.http.get<Persona>(endpoint + "/" + idPersona)
  }

  deletePersona(idPersona: number){
    return this.http.delete<Boolean>(endpoint + "/" + idPersona)
  }

  updatePersona(objPersona: Persona){
    var custom_header = new HttpHeaders();
    custom_header = custom_header.set("Content-Type", "application/json");

    return this.http.put<Boolean>(
      endpoint + "/modifica", 
      JSON.stringify(objPersona), 
      {headers: custom_header}
      )
  }

}
