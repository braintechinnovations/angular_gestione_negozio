import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartaDetailComponent } from './carta-detail.component';

describe('CartaDetailComponent', () => {
  let component: CartaDetailComponent;
  let fixture: ComponentFixture<CartaDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartaDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
