import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartaService } from '../carta.service';
import { PersonaService } from '../persona.service';

@Component({
  selector: 'app-carta-detail',
  templateUrl: './carta-detail.component.html',
  styleUrls: ['./carta-detail.component.css']
})
export class CartaDetailComponent implements OnInit {

  varId: any;
  varNegozio: any;
  varCodice: any;

  elencoPersone: any = []
  propSelezionato: any;

  constructor(
    private serviceCar: CartaService,
    private servicePer: PersonaService,
    private rottaAttiva: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.rottaAttiva.params.subscribe(
      (parametri) => {
        // console.log(parametri)

        this.serviceCar.findByIdCarta(parametri.cartaid).subscribe(
          (risultato) => {
            this.varId = risultato.id
            this.varCodice = risultato.numero;
            this.varNegozio = risultato.negozio

            this.cercaPersone()
          }
        )
      }
    )
  }

  cercaPersone(){
    this.servicePer.findAllPersona().subscribe(
      (risultato) => {
        this.elencoPersone = risultato;
      }
    )
  }

  associaCarta(){
    this.serviceCar.pairCartaAndProprietario(this.varId, this.propSelezionato).subscribe(
      (risultato) => {
        if(risultato){
          // alert("Tutto ok!");
          this.router.navigateByUrl("persona/dettaglio/" + this.propSelezionato);
        }
        else{
          alert("ohohoh e non sono babbo natale")
        }
      },
      (errore) => {
        alert(";(")
        console.log(errore);
      }
    )
  }

}
