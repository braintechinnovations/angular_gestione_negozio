import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CartaInsertComponent } from './carta-insert/carta-insert.component';
import { CartaDetailComponent } from './carta-detail/carta-detail.component';
import { CartaListComponent } from './carta-list/carta-list.component';
import { PersonaInsertComponent } from './persona-insert/persona-insert.component';
import { PersonaDetailComponent } from './persona-detail/persona-detail.component';
import { PersonaListComponent } from './persona-list/persona-list.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CartaInsertComponent,
    CartaDetailComponent,
    CartaListComponent,
    PersonaInsertComponent,
    PersonaDetailComponent,
    PersonaListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
