import { Component, OnInit } from '@angular/core';
import { CartaService } from '../carta.service';

@Component({
  selector: 'app-carta-list',
  templateUrl: './carta-list.component.html',
  styleUrls: ['./carta-list.component.css']
})
export class CartaListComponent implements OnInit {

  elencoCarte: any = [];

  constructor(private service: CartaService) { }

  ngOnInit(): void {

    this.service.findAllCarta().subscribe(
      (risultato) => {
        this.elencoCarte = risultato
      }
      //TODO: error
    )

  }

}
