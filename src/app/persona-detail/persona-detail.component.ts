import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartaService } from '../carta.service';
import { PersonaService } from '../persona.service';

@Component({
  selector: 'app-persona-detail',
  templateUrl: './persona-detail.component.html',
  styleUrls: ['./persona-detail.component.css']
})
export class PersonaDetailComponent implements OnInit {

  varId: any;
  varNome: any;
  varCognome: any;
  varCodFis: any;

  elencoCarte: any = [];

  constructor(
    private servicePer: PersonaService,
    private serviceCar: CartaService,
    private rottaAttiva: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {

    this.rottaAttiva.params.subscribe(
      (parametri) => {
        
        this.servicePer.findByIdPersona(parametri.personaid).subscribe(
          (risultato) => {
            this.varId = risultato.id;
            this.varNome = risultato.nome;
            this.varCognome = risultato.cognome;
            this.varCodFis = risultato.cod_fis;

            this.cercaCarte(this.varId)
          },
          (errore) => {
            alert("Errore")
            console.log(errore)
          }
        )

      }
    )

  }

  cercaCarte(propId: number){
    this.serviceCar.findCarteByProprietario(propId).subscribe(
      (risultato) => {
        this.elencoCarte = risultato;
      },
      (errore) => {

      }
    )
  }

  modificaPersona(){

  }

  eliminaPersona(){

  }

}
