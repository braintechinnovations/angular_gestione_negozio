import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartaDetailComponent } from './carta-detail/carta-detail.component';
import { CartaInsertComponent } from './carta-insert/carta-insert.component';
import { CartaListComponent } from './carta-list/carta-list.component';
import { PersonaDetailComponent } from './persona-detail/persona-detail.component';
import { PersonaInsertComponent } from './persona-insert/persona-insert.component';
import { PersonaListComponent } from './persona-list/persona-list.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "carta/lista",
    pathMatch: "full"
  },
  {
    path: "persona/inserisci",
    component: PersonaInsertComponent
  },
  {
    path: "persona/lista",
    component: PersonaListComponent
  },
  {
    path: "persona/dettaglio/:personaid",
    component: PersonaDetailComponent
  },
  {
    path: "carta/inserisci",
    component: CartaInsertComponent
  },
  {
    path: "carta/lista",
    component: CartaListComponent
  },
  {
    path: "carta/dettaglio/:cartaid",
    component: CartaDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
