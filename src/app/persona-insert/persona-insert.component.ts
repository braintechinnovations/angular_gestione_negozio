import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from '../persona';
import { PersonaService } from '../persona.service';

@Component({
  selector: 'app-persona-insert',
  templateUrl: './persona-insert.component.html',
  styleUrls: ['./persona-insert.component.css']
})
export class PersonaInsertComponent implements OnInit {

  varNome: any;
  varCognome: any;
  varCodFis: any;

  constructor(
      private service: PersonaService,
      private router: Router
    ) { }

  ngOnInit(): void {
  }

  inserisciPersona(){

    var temp = new Persona();
    temp.nome = this.varNome;
    temp.cognome = this.varCognome;
    temp.cod_fis = this.varCodFis;

    this.service.insertPersona(temp).subscribe(
      (risultato) => {
        if(risultato){
          alert("Ok!")
          this.router.navigateByUrl("persona/lista")
        }
        else{
          alert("Errore")
        }
      },
      (errore) => {
        alert("Errore grave")
        console.log(errore)
      }
    )
  }

}
