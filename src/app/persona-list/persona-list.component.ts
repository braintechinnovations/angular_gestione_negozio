import { Component, OnInit } from '@angular/core';
import { PersonaService } from '../persona.service';

@Component({
  selector: 'app-persona-list',
  templateUrl: './persona-list.component.html',
  styleUrls: ['./persona-list.component.css']
})
export class PersonaListComponent implements OnInit {

  elenco: any = [];

  constructor(private service: PersonaService) { }

  ngOnInit(): void {

    this.service.findAllPersona().subscribe(
      (risultato) => {
        this.elenco = risultato;
      },
      (errore) => {
        alert("Errore grave")
        console.log(errore)
      }
    )

  }

}
