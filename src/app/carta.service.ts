import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Carta } from './carta';

const endpoint = "http://localhost:8888/carta"

@Injectable({
  providedIn: 'root'
})
export class CartaService {

  constructor(private http: HttpClient) { }

  insertCarta(objCarta: Carta){
    var custom_header = new HttpHeaders();
    custom_header = custom_header.set("Content-Type", "application/json");

    return this.http.post<Carta>(
      endpoint + "/inserisci", 
      JSON.stringify(objCarta), 
      {headers: custom_header}
      );
  }

  findAllCarta(){
    return this.http.get(endpoint + "/")
  }

  findByIdCarta(idCarta: number){
    return this.http.get<Carta>(endpoint + "/" + idCarta)
  }

  deleteCarta(idCarta: number){
    return this.http.delete<Boolean>(endpoint + "/" + idCarta)
  }

  updateCarta(objCarta: Carta){
    var custom_header = new HttpHeaders();
    custom_header = custom_header.set("Content-Type", "application/json");

    return this.http.put<Boolean>(
      endpoint + "/modifica", 
      JSON.stringify(objCarta), 
      {headers: custom_header}
      )
  }

  findCarteByProprietario(proId: number){
    //http://localhost:8888/carta/proprietario/1
    return this.http.get(endpoint + "/proprietario/" + proId);
  }

  pairCartaAndProprietario(carId: number, proId: number){
    return this.http.get<Boolean>(endpoint + "/associa/" + carId + "/" + proId)
  }
}
